﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24.Models;

namespace Task24.Controllers
{
    // Magdeli Holmøy Asplin
    // 8/30/2019

    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.DateandTime = DateTime.Now;
            return View("MyFirstView");
        }

        public IActionResult SupInfo()
        {
            // Creating a list of supervisors that can be displayed in view
            List<Supervisor> Supervisors = new List<Supervisor>();

            // Adding supervisors to the list
            Supervisors.Add(new Supervisor { Name = "Per", Id = 1 });
            Supervisors.Add(new Supervisor { Name = "Paal", Id = 2 });
            Supervisors.Add(new Supervisor { Name = "Espen", Id = 3 });

            return View(Supervisors);
        }
    }
}
