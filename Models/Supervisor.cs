﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task24.Models
{
    // Magdeli Holmøy Asplin
    // 8/30/2019

    // This class can be used to create a supervisor with a name and an id
    public class Supervisor
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
